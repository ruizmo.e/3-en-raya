import JuegoInterfaz
import CodeWorld
import qualified Entrenamiento as E
import Control.Exception (catch, SomeException)
import System.Environment (getArgs)

main :: IO()
main = do
  args <- getArgs
  let (a:b:c:_) = if ((length(args)) == 3) then args else ["00","00","00"]
  let reps = read a
  let n_iters = read b
  let ia = if (read c == 1) then [True,False] else (if (read c == 2) then [False,True] else [False,False])
  qt <- E.cargaTabla 0
  if (read c /= 0 ) then do 
    E.iteracion qt n_iters 0
    qt <- bucle reps 1 qt n_iters
    interactionOf (inicio qt ia) (\_ s -> s) manejaEvento taab
  else do
    interactionOf (inicio qt ia) (\_ s -> s) manejaEvento taab

bucle :: (Num t1, Eq t1, Ord t, Fractional t) =>  t1 -> Integer -> E.QTable -> t -> IO E.QTable
bucle 0 _ qt n_iters = do
  return qt
bucle reps iter qt n_iters = do
  qt <- E.cargaTabla iter
  E.iteracion qt n_iters $ iter
  bucle (reps-1) (iter + 1) qt n_iters
